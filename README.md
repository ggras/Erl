# Eigen Robotics Library

Erl is a cross-platform, lightweight, header-only robotics library that makes use of the Eigen C++ linear algebra library. It provides the necessary tools for high-performance robotic software development, as well as a number of utility functions and classes useful in that context.

### Prerequisites

Eigen 3.3.

A compiler with C++11 support (MSVC 14.0 or higher on Windows).

### Installing

Erl is a header-only library. No compilation is required, simply add Erl to your include path.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.

