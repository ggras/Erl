/*
 * Copyright (c) 2013-2017 Gauthier Gras <gauthier.gras@gmail.com>
 * Copyright (c) 2013-2017 Konrad Leibrandt <konrad.lei@gmx.de>
 * Licensed under the MIT license. See the license file LICENSE.
*/

#ifndef ERL_DUALQUATERNION_H
#define ERL_DUALQUATERNION_H

#include "Erl/_forwards.h"

namespace Erl
{

typedef DualQuaternion<double> DualQuaterniond;
typedef DualQuaternion<float>  DualQuaternionf;

template<class T> struct ScrewParameters
{
    T theta;      /// Angle of rotation
    T dist;       /// Translation along the axis
    Vector3<T> l; /// Direction vector
    Vector3<T> m; /// Moment vector
};

template<class T> struct DualNumber
{
    T real;
    T dual;

    inline DualNumber ()  {}
    inline DualNumber (const DualNumber &Dn)
        : real(Dn.real)
        , dual(Dn.dual)
    {}
    inline DualNumber& operator = (const DualNumber &Dn)
    {
        real = Dn.real;
        dual = Dn.dual;
        return *this;
    }
    inline DualNumber (const T &Real, const T &Dual)
        : real(Real)
        , dual(Dual)
    {}
};
namespace details
{
    template <class T> class dualquaternion_determine_alignment { protected: Quaternion<T> qr; Quaternion<T> qd; };
}
template<class T> class
ERL_ALIGNAS_DUALQUATERNION
DualQuaternion
{

protected:

    Quaternion<T> m_qr;
    Quaternion<T> m_qd;

public:

// General constructors ===================================================================================

    inline DualQuaternion () {}

    inline DualQuaternion (const DualQuaternion &Dq)
        : m_qr(Dq.m_qr)
        , m_qd(Dq.m_qd)
    {}

    inline DualQuaternion& operator = (const DualQuaternion &Dq)
    {
        m_qr = Dq.m_qr;
        m_qd = Dq.m_qd;
        return *this;
    }

    inline DualQuaternion (const T &wr, const T &xr, const T &yr, const T &zr, const T &wd, const T &xd, const T &yd, const T &zd)
        : m_qr(wr, xr, yr, zr)
        , m_qd(wd, xd, yd, zd)
    {}

    inline DualQuaternion (const Quaternion<T> &Qr, const Quaternion<T> &Qd)
        : m_qr(Qr)
        , m_qd(Qd)
    {}

    template<typename OtherType> inline DualQuaternion (const Quaternion<OtherType> &Qr, const Quaternion<OtherType> &Qd)
        : m_qr(Qr.template cast<T>())
        , m_qd(Qd.template cast<T>())
    {}

    inline DualQuaternion (const Transform<T> &Tr)
        : m_qr(Tr.getQuaternion())
        , m_qd(((T)0.5)*Quaternion<T>(Tr.getTranslation())*m_qr)
    {}

    template<typename OtherType> inline DualQuaternion (const Transform<OtherType> &Tr)
        : m_qr(Tr.getQuaternion().template cast<T>())
        , m_qd(0.5*Quaternion<T>(Tr.getTranslation())*m_qr)
    {}

    inline DualQuaternion& operator = (const Transform<T> &Tr)
    {
        m_qr = Tr.getQuaternion().template cast<T>();
        m_qd = 0.5*Quaternion<T>(Tr.getTranslation())*m_qr;
    }

    template<typename OtherType> inline DualQuaternion& operator = (const Transform<OtherType> &Tr)
    {
        m_qr = Tr.getQuaternion();
        m_qd = 0.5*Quaternion<T>(Tr.getTranslation())*m_qr;
    }

    template<typename OtherType> inline DualQuaternion (const ScrewParameters<OtherType> &param)
    {
        T st2 = std::sin(param.theta/2);
        T ct2 = std::cos(param.theta/2);
        T pd2 = param.dist/2;
        m_qr = Quaternion<T>(ct2, param.l(0)*st2, param.l(1)*st2, param.l(2)*st2);
        m_qd = Quaternion<T>(-pd2*st2, param.m(0)*st2 + pd2*ct2*param.l(0), param.m(1)*st2 + pd2*ct2*param.l(1), param.m(2)*st2 + pd2*ct2*param.l(2));
    }

    template<typename OtherType> inline DualQuaternion& operator = (const ScrewParameters<OtherType> &param)
    {
        T st2 = std::sin(param.theta/2);
        T ct2 = std::cos(param.theta/2);
        T pd2 = param.dist/2;
        m_qr = Quaternion<T>(ct2, param.l(0)*st2, param.l(1)*st2, param.l(2)*st2);
        m_qd = Quaternion<T>(-pd2*st2, param.m(0)*st2 + pd2*ct2*param.l(0), param.m(1)*st2 + pd2*ct2*param.l(1), param.m(2)*st2 + pd2*ct2*param.l(2));
        return *this;
    }

// From Rotation Type ========================================================================================

    template<typename OtherType> inline DualQuaternion (const Eigen::Matrix<OtherType,3,3>& Rotation)
        : m_qr(Quaternion<T>(Rotation))
        , m_qd(0,0,0,0)
    {}

    template<typename OtherType> inline DualQuaternion (const Quaternion<OtherType> &quaternion)
        : m_qr(quaternion)
        , m_qd(0,0,0,0)
    {}

// From translation ======================================================================================

    template<typename OtherType> inline DualQuaternion (const Eigen::Matrix<OtherType,3,1>& Translation)
        : m_qr(1,0,0,0)
        , m_qd(0.5*Translation)
    {}

// From pointer ==========================================================================================

    template<typename OtherType> static inline DualQuaternion fromPointer(const OtherType *dataCombined)
    {
        return DualQuaternion( Quaternion<OtherType>::fromPointer(dataCombined),
                               Quaternion<OtherType>::fromPointer(dataCombined+4));
    }

    template<typename OtherType> static inline DualQuaternion fromPointer(const OtherType *dataQ1, const OtherType *dataQ2)
    {
        return DualQuaternion( Quaternion<OtherType>::fromPointer(dataQ1),
                               Quaternion<OtherType>::fromPointer(dataQ2));
    }

// Operators ===========================================================================================

    inline DualQuaternion operator + (const DualQuaternion &Dq) const
    {
        return DualQuaternion(m_qr + Dq.m_qr, m_qd + Dq.m_qd);
    }

    inline DualQuaternion operator - (const DualQuaternion &Dq) const
    {
        return DualQuaternion(m_qr - Dq.m_qr, m_qd - Dq.m_qd);
    }

    inline DualQuaternion& operator += (const DualQuaternion &Dq)
    {
        m_qr += Dq.m_qr;
        m_qd += Dq.m_qd;
        return *this;
    }

    inline DualQuaternion& operator -= (const DualQuaternion &Dq)
    {
        m_qr -= Dq.m_qr;
        m_qd -= Dq.m_qd;
        return *this;
    }

    inline DualQuaternion operator + () const
    {
        return *this;
    }

    inline DualQuaternion operator - () const
    {
        return DualQuaternion(-m_qr, -m_qd);
    }

    inline DualQuaternion operator * (const DualQuaternion &Dq) const
    {
        return DualQuaternion(m_qr*Dq.m_qr, m_qr*Dq.m_qd + m_qd*Dq.m_qr);
    }

    inline DualQuaternion& operator *= (const DualQuaternion &Dq)
    {
        m_qr = m_qr*Dq.m_qr;
        m_qd = m_qr*Dq.m_qd + m_qd*Dq.m_qr;
        return *this;
    }

    template<typename OtherType> inline DualQuaternion operator * (const OtherType &Scalar) const
    {
        return DualQuaternion(Scalar*m_qr, Scalar*m_qd);
    }

    template<typename OtherType> inline DualQuaternion operator *= (const OtherType &Scalar)
    {
        m_qr *= Scalar;
        m_qd *= Scalar;
        return *this;
    }

    template<typename OtherType> inline DualQuaternion operator / (const OtherType &Scalar) const
    {
        ERL_ASSERT(Scalar != 0 && "Dividing dual quaternion by zero");
        return DualQuaternion(m_qr/Scalar, m_qd/Scalar);
    }

    template<typename OtherType> inline DualQuaternion operator /= (const OtherType &Scalar)
    {
        ERL_ASSERT(Scalar != 0 && "Dividing dual quaternion by zero");
        m_qr /= Scalar;
        m_qd /= Scalar;
        return *this;
    }

    template<typename OtherType> inline friend DualQuaternion operator * (const OtherType &Scalar, const DualQuaternion &Dq)
    {
        return Dq*Scalar;
    }

    inline bool operator == (const DualQuaternion &Dq) const
    {
        return (m_qr == Dq.m_qr && m_qd == Dq.m_qd);
    }

    inline bool operator != (const DualQuaternion &Dq) const
    {
        return (m_qr != Dq.m_qr || m_qd != Dq.m_qd);
    }

    inline friend std::ostream& operator << (std::ostream &out, const DualQuaternion &Dq)
    {
        out << Dq.m_qr << " " << Dq.m_qd;
        return out;
    }

// Accessors ===========================================================================================

    inline Quaternion<T> Qr () const
    {
        return m_qr;
    }

    inline Quaternion<T> Qd () const
    {
        return m_qd;
    }

    inline void Qr (const Quaternion<T> &Qin)
    {
        m_qr = Qin;
    }

    inline void Qd (const Quaternion<T> &Qin)
    {
        m_qd = Qin;
    }

// Getters =============================================================================================

    template<int MajorType> inline Transform<T, MajorType> getTransform () const
    {
        return Transform<T, MajorType>(m_qr, T(2)*(m_qd*m_qr.conj()).getVector());
    }

    inline Vector3<T> getVector () const
    {
        return m_qd.getVector();
    }

    inline ScrewParameters<T> getScrewParameters() const
    {
        T wr = m_qr.w();
        T wd = m_qd.w();
        Vector3<T> vr = m_qr.getVector();
        Vector3<T> vd = m_qd.getVector();
        T vrn = vr.norm();
        ERL_ASSERT(vrn != 0 && "real vector norm = 0 for screw parameters");

        ScrewParameters<T> param;
        param.theta = 2*std::acos(m_qr.wr);
        param.dist = -2*wd/vrn;
        param.l = vr/vrn;
        param.m = (vd - param.l*param.dist*wr/2)/vrn;

        return param;
    }

// Functions ============================================================================================

    /// The functions below use the name "norm" but refer to the magnitude.

    inline DualNumber<T> norm () const
    {
        T realNorm = m_qr.norm();
        ERL_ASSERT(realNorm != 0 && "Dual quaternion real part norm = 0");
        return DualNumber<T>(realNorm, m_qr.dot(m_qd)/realNorm);
    }

    inline DualQuaternion normalized () const
    {
        T realNorm = m_qr.norm();
        ERL_ASSERT(realNorm != 0 && "Dual quaternion real part norm = 0");
        T invNorm = 1.0/realNorm;
        Quaternion<T> qar, qad;
        qar = m_qr*invNorm;
        qad = m_qd*invNorm;
        qad -= qar*(qar.dot(qad));

        return DualQuaternion(qar, qad);
    }

    inline void normalize ()
    {
        T realNorm = m_qr.norm();
        ERL_ASSERT(realNorm != 0 && "Dual quaternion real part norm = 0");
        T invNorm = 1.0/realNorm;
        m_qr *= invNorm;
        m_qd *= invNorm;
        m_qd -= m_qr*(m_qr.dot(m_qd));
    }

    /// Inverse not allowed, conjugate should be used with unit quaternions instead.

    inline DualQuaternion conj () const
    {
        return DualQuaternion(m_qr.conj(), m_qd.conj());
    }

    template <typename OtherType> inline Vector3<T> transformVector (const Eigen::Matrix<OtherType,3,1>& vector) const
    {
        ERL_ASSERT(m_qr.norm() != 1 && "Dual quaternion not a unit dual quaternion");
        DualQuaternion result = (*this)*DualQuaternion(vector)*conj();
        return result.getVector();
    }

    template <typename OtherType> inline DualQuaternion<OtherType> cast () const
    {
        return DualQuaternion<OtherType> (m_qr, m_qd);
    }

    inline static DualQuaternion Identity ()
    {
        return DualQuaternion(1,0,0,0,0,0,0,0);
    }

    inline static DualQuaternion Random ()
    {
        return DualQuaternion(Quaternion<T>::Random(), Quaternion<T>::Random());
    }

    inline static DualQuaternion Zero ()
    {
        return DualQuaternion(0,0,0,0,0,0,0,0);
    }

};

}

#endif
