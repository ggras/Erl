/*
 * Copyright (c) 2013-2017 Gauthier Gras <gauthier.gras@gmail.com>
 * Copyright (c) 2013-2017 Konrad Leibrandt <konrad.lei@gmx.de>
 * Licensed under the MIT license. See the license file LICENSE.
*/

#ifndef ERL_MOVINGFILTER_H
#define ERL_MOVINGFILTER_H

#include <vector>

namespace Erl
{

template<class T> class MovingFilter
{

public:

    inline MovingFilter(unsigned s, const T & initvalue = T())
        : m_size(s),
          m_counter(0),
          m_full(false)
    {
        m_data.resize(m_size, initvalue);
        m_dOut = s*m_data[0];
    }

    inline ~MovingFilter()
    {
    }

    inline void add (const T & Din)
    {
        m_dOut = m_dOut + (Din - m_data[m_counter]);
        m_data[m_counter] = Din;
        m_counter++;
        if (m_counter == m_size)
        {
            m_counter = 0;
            m_full = true;
        }
    }

    inline T get () const
    {
        if (m_full)
        {
            return m_dOut/m_size;
        }
        else
        {
            if (m_counter >= 1)
                return m_data[m_counter - 1];
            else
                return m_data[0];
        }
    }

    inline bool isfull () const
    {
        return m_full;
    }

    inline unsigned getCounter () const
    {
        return m_counter;
    }

    inline std::vector<T> getData () const
    {
        return m_data;
    }

protected:

    int m_size, m_counter;
    bool m_full;
    std::vector<T> m_data;
    T m_dOut;
};

}

#endif
