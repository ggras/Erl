/*
 * Copyright (c) 2013-2017 Gauthier Gras <gauthier.gras@gmail.com>
 * Copyright (c) 2013-2017 Konrad Leibrandt <konrad.lei@gmx.de>
 * Licensed under the MIT license. See the license file LICENSE.
*/

#ifndef ERL_REQUESTMANAGER_H
#define ERL_REQUESTMANAGER_H

#include <list>
#include <tuple>
#include <mutex>
#include <algorithm>
#include <atomic>
#include <thread>

namespace Erl
{

template<typename T> class RequestManager_t;
typedef RequestManager_t<std::mutex> RequestManager;

namespace details
{

/// Non-member function callback

template <typename F, typename Tuple, bool done, int total, int... params> struct rqm_call_impl
{
    static void call(F f, Tuple && t)
    {
        rqm_call_impl<F, Tuple, total == 1 + sizeof...(params), total, params..., sizeof...(params)>::call(f, std::forward<Tuple>(t));
    }
};

template <typename F, typename Tuple, int total, int... params> struct rqm_call_impl<F, Tuple, true, total, params...>
{
    static void call(F f, Tuple && t)
    {
        f(std::get<params>(std::forward<Tuple>(t))...);
    }
};

template <typename F, typename Tuple> void rqm_call(F f, Tuple && t)
{
    typedef typename std::decay<Tuple>::type ttype;
    rqm_call_impl<F, Tuple, 0 == std::tuple_size<ttype>::value, std::tuple_size<ttype>::value>::call(f, std::forward<Tuple>(t));
}

/// Member function callback

template <typename F, typename I, typename Tuple, bool done, int total, int... params> struct rqm_call_impl_class
{
    static void call(F f, I *i, Tuple && t)
    {
        rqm_call_impl_class<F, I, Tuple, total == 1 + sizeof...(params), total, params..., sizeof...(params)>::call(f, i, std::forward<Tuple>(t));
    }
};

template <typename F, typename I, typename Tuple, int total, int... params> struct rqm_call_impl_class<F, I, Tuple, true, total, params...>
{
    static void call(F f, I *i, Tuple && t)
    {
        (i->*f)(std::get<params>(std::forward<Tuple>(t))...);
    }
};

template <typename F, typename I, typename Tuple> void rqm_call_class(F f, I *i, Tuple && t)
{
    typedef typename std::decay<Tuple>::type ttype;
    rqm_call_impl_class<F, I, Tuple, 0 == std::tuple_size<ttype>::value, std::tuple_size<ttype>::value>::call(f, i, std::forward<Tuple>(t));
}

class RequestContainerBase
{

public:

    inline virtual ~RequestContainerBase() {}
    virtual void handleRequest() = 0;
};

template<class F, typename... Types> class RequestContainer : public RequestContainerBase
{

public:

    inline virtual ~RequestContainer() {}

    inline void createRequest(F function, Types&&... args)
    {
        m_func = function;
        m_params = std::make_tuple(std::forward<Types>(args)...);
    }

    inline void handleRequest()
    {
        rqm_call(m_func, m_params);
    }

private:

    std::tuple<typename std::decay<Types>::type...> m_params;
    F m_func;
};

template<class F, class Class, typename... Types> class RequestClassContainer : public RequestContainerBase
{

public:

    inline virtual ~RequestClassContainer() {}

    inline void createRequest(F function, Class *classInstance, Types&&... args)
    {
        m_func = function;
        m_params = std::make_tuple(std::forward<Types>(args)...);
        m_instance = classInstance;
    }

    inline void handleRequest()
    {
        rqm_call_class(m_func, m_instance, m_params);
    }

private:

    std::tuple<typename std::decay<Types>::type...> m_params;
    F m_func;
    Class *m_instance;
};

template <typename lockable>
class ThreadPoolManager
{

public:

    ThreadPoolManager () : m_numThreads(0), m_doneJobs(0) {}
    void run(Erl::RequestManager_t<lockable> &rqm, const int &n, const int &waitSleep_ms = 10);

private:

    void runRequest(Erl::RequestManager_t<lockable> &rqm);

    std::atomic<int> m_numThreads;
    std::atomic<int> m_doneJobs;
};

}

template <typename lockable>
class RequestManager_t
{

public:

     inline RequestManager_t()
        : m_requests(new std::list<details::RequestContainerBase*>()) {}

    inline ~RequestManager_t()
    {
        std::list<details::RequestContainerBase*>::iterator it;
        for (it = m_requests->begin(); it != m_requests->end(); ++it)
            delete (*it);
        delete m_requests;
    }

    template<class F, typename... Types> inline void createRequest(F function, Types&&... args)
    {
        details::RequestContainer<F, Types...> *container = new details::RequestContainer<F, Types...>;
        container->createRequest(function, std::forward<Types>(args)...);

        m_requestLock.lock();
        m_requests->push_back(container);
        m_requestLock.unlock();
    }

    template<class F, class Class, typename... Types> inline void createRequest(F function, Class *classInstance, Types&&... args)
    {
        details::RequestClassContainer<F, Class, Types...> *container = new details::RequestClassContainer<F, Class, Types...>;
        container->createRequest(function, classInstance, std::forward<Types>(args)...);

        m_requestLock.lock();
        m_requests->push_back(container);
        m_requestLock.unlock();
    }

    inline void handleAllRequests()
    {
        m_requestLock.lock();
        if (m_requests->size() == 0)
        {
            m_requestLock.unlock();
            return;
        }
        std::list<details::RequestContainerBase*> *storedRequest = m_requests;
        m_requests = new std::list<details::RequestContainerBase*>();
        m_requestLock.unlock();


        std::list<details::RequestContainerBase*>::iterator it;
        for (it = storedRequest->begin(); it != storedRequest->end(); ++it)
        {
            (*it)->handleRequest();
            delete (*it);
        }

        delete storedRequest;
    }

    inline void handleRequest()
    {
        m_requestLock.lock();
        if (m_requests->size() == 0)
        {
            m_requestLock.unlock();
            return;
        }
        details::RequestContainerBase *storedRequest = m_requests->front();
        m_requests->pop_front();
        m_requestLock.unlock();

        storedRequest->handleRequest();
        delete storedRequest;
    }

    inline void handleAllRequests_threadPool(const int &numThreads, const int &waitSleep_ms = 10)
    {
        details::ThreadPoolManager<lockable> thp;
        thp.run(*this, numThreads, waitSleep_ms);
    }

    inline void handleAllRequestsBlocking()
    {
        m_handleLock.lock();
        handleAllRequests();
        m_handleLock.unlock();
    }

    inline void handleRequestBlocking()
    {
        m_handleLock.lock();
        handleRequest();
        m_handleLock.unlock();
    }

    inline int getNumberOfRequests() const
    {
        m_requestLock.lock();
        int temp = m_requests->size();
        m_requestLock.unlock();
        return temp;
    }

    inline void deleteFirstRequests(const unsigned &numberToDelete)
    {
        m_requestLock.lock();
        int toClear = std::min(numberToDelete, (unsigned)m_requests->size());
        std::list<details::RequestContainerBase*>::iterator it = m_requests->begin();
        for (int i = 0; i < toClear; ++i, ++it)
        {
            delete (*it);
        }
        m_requests->erase(m_requests->begin(), it);
        m_requestLock.unlock();
    }

    inline void deleteLastRequests(const unsigned &numberToDelete)
    {
        m_requestLock.lock();
        int toClear = std::min(numberToDelete, (unsigned)m_requests->size());
        std::list<details::RequestContainerBase*>::iterator it = m_requests->end();
        for (int i = 0; i < toClear; ++i, --it)
        {
            delete (*it);
        }
        m_requests->erase(it, m_requests->end());
        m_requestLock.unlock();
    }

protected:

    std::list<details::RequestContainerBase*> *m_requests;
    mutable lockable m_requestLock;
    mutable lockable m_handleLock;
};



namespace details
{

template <typename lockable>
inline void ThreadPoolManager<lockable>::run(Erl::RequestManager_t<lockable> &rqm, const int &n, const int &waitSleep_ms)
{
    int numJobs = rqm.getNumberOfRequests();
    m_doneJobs = 0;
    while (m_doneJobs < numJobs)
    {
        if (m_numThreads < n && (m_numThreads + m_doneJobs) < numJobs)
        {
            m_numThreads++;
            std::thread th(&ThreadPoolManager<lockable>::runRequest, this, std::ref(rqm));
            th.detach();
        }
        else
            std::this_thread::sleep_for(std::chrono::duration<double, std::milli>(waitSleep_ms));
    }
}

template <typename lockable>
inline void ThreadPoolManager<lockable>::runRequest(Erl::RequestManager_t<lockable> &rqm)
{
    rqm.handleRequest();
    m_doneJobs++;
    m_numThreads--;
}


}

}

#endif
