/*
 * Copyright (c) 2013-2018 Konrad Leibrandt <konrad.lei@gmx.de>
 * Copyright (c) 2013-2018 Gauthier Gras <gauthier.gras@gmail.com>
 * Licensed under the MIT license. See the license file LICENSE.
*/

#ifndef ERL_LOCKALL_H
#define ERL_LOCKALL_H

#include <cstddef>
#include <mutex>

//! ********************************************************************************************************
//! Erl::RT::lock_all: to be used to lock multiple mutexes simulatously while ensuring to avoid deadlocks.
//! Usage:
//!     using mutex_t = std::mutex;
//!     using lock_t = std::lock_guard<mutex_t>;
//!     mutex_t m0, m1, m2;
//!     Erl::RT::LockAll<lock_t, lock_t, lock_t> lock(m0, m1, m2);
//! ********************************************************************************************************

namespace Erl
{
namespace RT
{
namespace details
{
template<std::size_t n, class ...lock_ts>
class LockAdopt;

template<std::size_t n, class lock_t>
class LockAdopt<n, lock_t> : public lock_t
{

public:
    template <class mutex>
    inline LockAdopt(mutex &m)
        : lock_t(m, std::adopt_lock)
    {}

    ~LockAdopt() = default;
};


template<std::size_t n, class lock_t, class ...lock_ts>
class LockAdopt<n, lock_t, lock_ts...> : public LockAdopt<n, lock_t>, LockAdopt<n-1, lock_ts...>
{
public:
    template <class mutex_t, class ...mutex_ts>
    inline LockAdopt(mutex_t& m, mutex_ts&... ms)
        : LockAdopt<n, lock_t>(m)
        , LockAdopt<n-1, lock_ts...>(ms...)
    {}

    ~LockAdopt() = default;
};

}

template <class ...lock_t>
class LockAll
{
  using lock_containter = details::LockAdopt<sizeof...(lock_t), lock_t...>;

public:
    template <class ... mutex_ts>
    inline LockAll(mutex_ts&... ms)
        : LockAll( (std::lock(ms...), nullptr), ms...)
    {}

    ~LockAll() = default;

protected:
    template <class ...mutex_ts>
    inline LockAll(std::nullptr_t, mutex_ts&... ms)
        : m_locks(ms...)
    {}

protected:
    lock_containter m_locks;

};

template <class lock_t>
class LockAll<lock_t>
{
public:
    template <class mutex_t>
    inline LockAll(mutex_t& m)
        : m_locks(m)
    {}

    ~LockAll() = default;

protected:
    lock_t m_locks;

};

}
}
#endif
