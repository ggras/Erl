/*
 * Copyright (c) 2013-2017 Gauthier Gras <gauthier.gras@gmail.com>
 * Copyright (c) 2013-2017 Konrad Leibrandt <konrad.lei@gmx.de>
 * Licensed under the MIT license. See the license file LICENSE.
*/

/// Core class for homogeneous transforms. Do not include manually.
#ifndef ERL_TRANSFORM_H
    #error "Do not include _transformCore.h -> include transform.h instead"
#endif

#if (ERL_MAJOR_TYPE == ERL_MAJOR_TYPE_GENERIC)

#define ERL_TEMPLATE_ARGS class T, int MajorType
#define ERL_MAJOR_TYPE_VALUE MajorType
#define ERL_CLASS_ARGS

#elif (ERL_MAJOR_TYPE == ERL_MAJOR_TYPE_COL)

#define ERL_TEMPLATE_ARGS class T
#define ERL_MAJOR_TYPE_VALUE ColMajor
#define ERL_CLASS_ARGS <T, ERL_MAJOR_TYPE_VALUE>

#elif (ERL_MAJOR_TYPE == ERL_MAJOR_TYPE_ROW)

#define ERL_TEMPLATE_ARGS class T
#define ERL_MAJOR_TYPE_VALUE RowMajor
#define ERL_CLASS_ARGS <T, ERL_MAJOR_TYPE_VALUE>

#endif

namespace Erl
{
namespace details
{
    template <ERL_TEMPLATE_ARGS> class transform_determine_alignment ERL_CLASS_ARGS { protected: Rotmat<T, ERL_MAJOR_TYPE_VALUE> rotation; Vector3<T> translation; };
}
template <ERL_TEMPLATE_ARGS> class
ERL_ALIGNAS_TRANSFORM
Transform ERL_CLASS_ARGS
{

protected:

    Rotmat<T, ERL_MAJOR_TYPE_VALUE> m_rotation;

    Vector3<T> m_translation;

public:

// General constructors ===================================================================================

    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    inline Transform()
        : m_rotation()
        , m_translation()
    {}

    template<typename OtherType> inline Transform (const Eigen::Matrix<OtherType,3,4>& other)
        : m_rotation(other.template topLeftCorner<3,3>())
        , m_translation(other.template topRightCorner<3,1>())
    {}

    template<typename OtherType> inline Transform & operator= (const Eigen::Matrix<OtherType,3,4>& other)
    {
        m_rotation = other.template topLeftCorner<3,3>();
        m_translation = other.template topRightCorner<3,1>();
        return *this;
    }

    template<typename OtherType> inline Transform (const Eigen::Matrix<OtherType,4,4>& other)
        : m_rotation(other.template topLeftCorner<3,3>())
        , m_translation(other.template topRightCorner<3,1>())
    {}

    template<typename OtherType> inline Transform & operator= (const Eigen::Matrix<OtherType,4,4>& other)
    {
        m_rotation = other.template topLeftCorner<3,3>();
        m_translation = other.template topRightCorner<3,1>();
        return *this;
    }

    template<int OtherMajor> inline Transform (const Transform<T,OtherMajor> &Tr)
        : m_rotation(Tr.getRotation())
        , m_translation(Tr.getTranslation())
    {}

    template<int OtherMajor> inline Transform & operator = (const Transform<T,OtherMajor> &Tr)
    {
        m_rotation = Tr.getRotation();
        m_translation = Tr.getTranslation();
        return *this;
    }

    inline Transform (const T& _00, const T& _01, const T& _02, const T& _03, const T& _10, const T& _11, const T& _12, const T& _13, const T& _20, const T& _21, const T& _22, const T& _23)
        : m_rotation(_00, _01, _02, _10, _11, _12, _20, _21, _22)
        , m_translation(_03, _13, _23)
    {}

//  Constructors from vector type ==========================================================================

    template<typename OtherType> inline Transform (const Eigen::Matrix<OtherType,3,1>& Translation)
        : m_rotation(Eigen::Matrix<T,3,3>::Identity())
        , m_translation(Translation)
    {}

//  Constructors from rotation type =========================================================================

    template<typename OtherType> inline Transform (const Eigen::Matrix<OtherType,3,3>& Rotation)
        : m_rotation(Rotation)
        , m_translation(0,0,0)
    {}

    template<typename OtherType> inline Transform (const Quaternion<OtherType> &quaternion)
        : m_rotation(Rotmat<T, ERL_MAJOR_TYPE_VALUE>(quaternion))
        , m_translation(0,0,0)
    {}

//  Constructors from both types ============================================================================

    template<typename OtherType1, typename OtherType2> inline Transform (const Eigen::Matrix<OtherType1, 3, 3>& Rotation, const Eigen::Matrix<OtherType2, 3, 1>& Translation)
        : m_rotation(Rotation)
        , m_translation(Translation)
    {}

    template<typename OtherType1, typename OtherType2> inline Transform (const Quaternion<OtherType1> &quaternion, const Eigen::Matrix<OtherType2, 3, 1>& Translation)
        : m_rotation(Rotmat<T, ERL_MAJOR_TYPE_VALUE>(quaternion))
        , m_translation(Translation)
    {}

//  Static constructors from pointer ========================================================================

#if (ERL_MAJOR_TYPE == ERL_MAJOR_TYPE_GENERIC)
    template<typename OtherType> inline static Transform fromPointer (const OtherType *data, int dataMajorType);
#elif (ERL_MAJOR_TYPE == ERL_MAJOR_TYPE_COL)
    template<typename OtherType> inline static Transform fromPointer (const OtherType *data, int dataMajorType)
    {
        Transform returnTransform;
        if (dataMajorType == Erl::ColMajor)
        {
            returnTransform.m_rotation.data()[0] = data[0];
            returnTransform.m_rotation.data()[1] = data[1];
            returnTransform.m_rotation.data()[2] = data[2];

            returnTransform.m_rotation.data()[3] = data[3];
            returnTransform.m_rotation.data()[4] = data[4];
            returnTransform.m_rotation.data()[5] = data[5];

            returnTransform.m_rotation.data()[6] = data[6];
            returnTransform.m_rotation.data()[7] = data[7];
            returnTransform.m_rotation.data()[8] = data[8];

            returnTransform.m_translation.data()[0] = data[9];
            returnTransform.m_translation.data()[1] = data[10];
            returnTransform.m_translation.data()[2] = data[11];
        }
        else
        {
            returnTransform.m_rotation.data()[0] = data[0];
            returnTransform.m_rotation.data()[1] = data[4];
            returnTransform.m_rotation.data()[2] = data[8];

            returnTransform.m_rotation.data()[3] = data[1];
            returnTransform.m_rotation.data()[4] = data[5];
            returnTransform.m_rotation.data()[5] = data[9];

            returnTransform.m_rotation.data()[6] = data[2];
            returnTransform.m_rotation.data()[7] = data[6];
            returnTransform.m_rotation.data()[8] = data[10];

            returnTransform.m_translation.data()[0] = data[3];
            returnTransform.m_translation.data()[1] = data[7];
            returnTransform.m_translation.data()[2] = data[11];
        }
        return returnTransform;
    }
#elif (ERL_MAJOR_TYPE == ERL_MAJOR_TYPE_ROW)
    template<typename OtherType> inline static Transform fromPointer (const OtherType *data, int dataMajorType)
    {
        Transform returnTransform;
        if (dataMajorType == Erl::ColMajor)
        {
            returnTransform.m_rotation.data()[0] = data[0];
            returnTransform.m_rotation.data()[1] = data[1];
            returnTransform.m_rotation.data()[2] = data[2];
            returnTransform.m_translation.data()[0] = data[3];

            returnTransform.m_rotation.data()[3] = data[4];
            returnTransform.m_rotation.data()[4] = data[5];
            returnTransform.m_rotation.data()[5] = data[6];
            returnTransform.m_translation.data()[1] = data[7];

            returnTransform.m_rotation.data()[6] = data[8];
            returnTransform.m_rotation.data()[7] = data[9];
            returnTransform.m_rotation.data()[8] = data[10];
            returnTransform.m_translation.data()[2] = data[11];
        }
        else
        {
            returnTransform.m_rotation.data()[0] = data[0];
            returnTransform.m_rotation.data()[1] = data[3];
            returnTransform.m_rotation.data()[2] = data[6];
            returnTransform.m_translation.data()[0] = data[9];

            returnTransform.m_rotation.data()[3] = data[1];
            returnTransform.m_rotation.data()[4] = data[4];
            returnTransform.m_rotation.data()[5] = data[7];
            returnTransform.m_translation.data()[1] = data[10];

            returnTransform.m_rotation.data()[6] = data[2];
            returnTransform.m_rotation.data()[7] = data[5];
            returnTransform.m_rotation.data()[8] = data[8];
            returnTransform.m_translation.data()[2] = data[11];
        }
        return returnTransform;
    }
#endif

    template<typename OtherType> inline static Transform fromEigenMatrix(const Eigen::MatrixBase<OtherType> &M)
    {
        Transform ret;
        ret.m_rotation    = M.template block<3,3>(0,0);
        ret.m_translation = M.template block<3,1>(0,3);
        return ret;
    }

// Operators =======================================================================================================

    inline Transform operator * (const T &scalar) const
    {
        return Transform(scalar*m_rotation, scalar*m_translation);
    }

    inline Transform operator / (const T &scalar) const
    {
        return Transform(m_rotation/scalar, m_translation/scalar);
    }

    inline Transform & operator *= (const T &scalar)
    {
        m_rotation *= scalar;
        m_translation *= scalar;
        return *this;
    }

    inline Transform & operator /= (const T &scalar)
    {
        m_rotation /= scalar;
        m_translation /= scalar;
        return *this;
    }

    inline friend Transform operator * (const T &scalar, const Transform Tr)
    {
        return Tr*scalar;
    }

    inline Transform operator * (const Transform &Tr) const
    {
        Transform newTr;
        newTr.m_rotation = m_rotation*Tr.m_rotation;
        newTr.m_translation = m_rotation*Tr.m_translation + m_translation;
        return newTr;
    }

    inline Transform & operator *= (const Transform &Tr)
    {
        m_translation += m_rotation*Tr.m_translation;
        m_rotation     = m_rotation*Tr.m_rotation;
        return *this;
    }

    inline Vector3<T> operator * (const Vector3<T> &Vin) const
    {
        return Vector3<T>(m_rotation*Vin + m_translation);
    }

    inline Transform operator + (const Transform &Tr) const
    {
        Transform newTr;
        newTr.m_rotation = m_rotation + Tr.m_rotation;
        newTr.m_translation = m_translation + Tr.m_translation;
        return newTr;
    }

    inline Transform operator - (const Transform &Tr) const
    {
        Transform newTr;
        newTr.m_rotation = m_rotation - Tr.m_rotation;
        newTr.m_translation = m_translation - Tr.m_translation;
        return newTr;
    }

    inline Transform operator + () const
    {
        return *this;
    }

    inline Transform operator - () const
    {
        Transform newTr;
        newTr.m_rotation = - m_rotation ;
        newTr.m_translation = - m_translation;
        return newTr;
    }

    inline Transform & operator += (const Transform &Tr)
    {
        m_rotation += Tr.m_rotation;
        m_translation += Tr.m_translation;
        return *this;
    }

    inline Transform & operator -= (const Transform &Tr)
    {
        m_rotation -= Tr.m_rotation;
        m_translation -= Tr.m_translation;
        return *this;
    }

    template<typename OtherType> inline friend Transform operator * (const Rotmat<OtherType> &Rot, const Transform Tr)
    {
        return Transform<T> (Rotmat<T>(Rot*Tr.getRotationReference()), Vector3<T>(Rot*Tr.getTranslationReference()));
    }

    inline T operator () (const unsigned &r, const unsigned &c) const
    {
        if (c < 3)
            return m_rotation(r, c);
        else
            return m_translation(r);
    }

    inline T& operator () (const unsigned &r, const unsigned &c)
    {
        if (c < 3)
            return m_rotation(r, c);
        else
            return m_translation(r);
    }


    inline bool operator == (const Transform &Tr) const
    {
        return (m_rotation == Tr.m_rotation) && (m_translation == Tr.m_translation);
    }

    inline bool operator != (const Transform &Tr) const
    {
        return (m_rotation != Tr.m_rotation) || (m_translation != Tr.m_translation);
    }

    inline friend std::ostream & operator<< (std::ostream& os, const Transform &obj)
    {
        Eigen::Matrix<T,3,4> tmp;
        tmp.template block<3,3>(0,0) = obj.getRotationReference();
        tmp.template block<3,1>(0,3) = obj.getTranslationReference();
        os << tmp << std::endl;
        return os;
    }

// Getters =======================================================================================================

    inline Quaternion<T> getQuaternion () const
    {
        return m_rotation.getQuaternion();
    }

    inline Rotmat<T, ERL_MAJOR_TYPE_VALUE> getRotation () const
    {
        return m_rotation;
    }

    inline Vector3<T> getTranslation () const
    {
        return m_translation;
    }

    inline Rotmat<T, ERL_MAJOR_TYPE_VALUE> & getRotationReference ()
    {
        return m_rotation;
    }

    inline Rotmat<T, ERL_MAJOR_TYPE_VALUE> const & getRotationReference () const
    {
        return m_rotation;
    }

    inline Vector3<T> & getTranslationReference ()
    {
        return m_translation;
    }

    inline Vector3<T> const & getTranslationReference () const
    {
        return m_translation;
    }

#if (ERL_MAJOR_TYPE == ERL_MAJOR_TYPE_GENERIC)
    inline T* getData (T* data, const int &dataMajorType) const;
#elif (ERL_MAJOR_TYPE == ERL_MAJOR_TYPE_COL)
    inline T* getData (T* data, const int &dataMajorType) const
    {
        if (dataMajorType == ColMajor)
        {
            memcpy(data  ,m_rotation.data()   ,sizeof(T)*9);
            memcpy(data+9,m_translation.data(),sizeof(T)*3);
        }
        else
        {
            data[0] = m_rotation.data()[0];
            data[1] = m_rotation.data()[3];
            data[2] = m_rotation.data()[6];
            data[3] = m_translation.data()[0];

            data[4] = m_rotation.data()[1];
            data[5] = m_rotation.data()[4];
            data[6] = m_rotation.data()[7];
            data[7] = m_translation.data()[1];

            data[8]  = m_rotation.data()[2];
            data[9]  = m_rotation.data()[5];
            data[10] = m_rotation.data()[8];
            data[11] = m_translation.data()[2];
        }
        return data;
    }
#elif (ERL_MAJOR_TYPE == ERL_MAJOR_TYPE_ROW)
    inline T* getData (T* data, const int &dataMajorType) const
    {
        if (dataMajorType == ColMajor)
        {
            data[0] = m_rotation.data()[0];
            data[1] = m_rotation.data()[3];
            data[2] = m_rotation.data()[6];

            data[3] = m_rotation.data()[1];
            data[4] = m_rotation.data()[4];
            data[5] = m_rotation.data()[7];

            data[6] = m_rotation.data()[2];
            data[7] = m_rotation.data()[5];
            data[8] = m_rotation.data()[8];

            data[8]  = m_translation.data()[0];
            data[10] = m_translation.data()[1];
            data[11] = m_translation.data()[2];
        }
        else
        {
            data[0] = m_rotation.data()[0];
            data[1] = m_rotation.data()[1];
            data[2] = m_rotation.data()[2];
            data[3] = m_translation.data()[0];

            data[4] = m_rotation.data()[3];
            data[5] = m_rotation.data()[4];
            data[6] = m_rotation.data()[5];
            data[7] = m_translation.data()[1];

            data[8]  = m_rotation.data()[6];
            data[9]  = m_rotation.data()[7];
            data[10] = m_rotation.data()[8];
            data[11] = m_translation.data()[2];
        }
        return data;
    }
#endif

    inline Vector3<T> getColumn0 () const
    {
        return m_rotation.getColumn0();
    }

    inline Vector3<T> getColumn1 () const
    {
        return m_rotation.getColumn1();
    }

    inline Vector3<T> getColumn2 () const
    {
        return m_rotation.getColumn2();
    }

    inline Vector6<T> getVector6_RPY () const
    {
        return Vector6<T>(m_translation, m_rotation.getRPY());
    }

    inline Vector6<T> getVector6_Euler (const Axis &axis1, const Axis &axis2, const Axis &axis3) const
    {
        return Vector6<T>(m_translation, m_rotation.getEuler(axis1, axis2, axis3));
    }

    inline T getX() const
    {
        return m_translation.x();
    }

    inline T getY() const
    {
        return m_translation.y();
    }

    inline T getZ() const
    {
        return m_translation.z();
    }

    inline Eigen::Matrix<T, 4, 4, ERL_MAJOR_TYPE_VALUE> getEigenMatrix() const
    {
        Eigen::Matrix<T, 4, 4, ERL_MAJOR_TYPE_VALUE> ret = Eigen::Matrix<T, 4, 4, ERL_MAJOR_TYPE_VALUE>::Identity();
        ret.template block<3,3>(0,0) = m_rotation   ;
        ret.template block<3,1>(0,3) = m_translation;
        return ret;
    }

    template<typename OtherType> inline void toEigen(Eigen::MatrixBase<OtherType> &M) const
    {
        M.template block<3,3>(0,0) = m_rotation   ;
        M.template block<3,1>(0,3) = m_translation;
    }

// Setters =======================================================================================================

    template<typename OtherType> inline void setTranslation (const Eigen::Matrix<OtherType,3,1> &Translation)
    {
        m_translation = Translation;
    }

    template<typename OtherType> inline void setRotation (const Eigen::Matrix<OtherType,3,3> &Rotation)
    {
        m_rotation = Rotation;
    }

    template<typename OtherType> inline void setRotation (const Quaternion<OtherType> &quaternion)
    {
        m_rotation = quaternion.template getMatrix<ERL_MAJOR_TYPE_VALUE>();
    }

    template <typename OtherType> inline void setColumn0 (const Eigen::Matrix<OtherType,3,1> &vector)
    {
        m_rotation.setColumn0(vector);
    }

    template <typename OtherType> inline void setColumn1 (const Eigen::Matrix<OtherType,3,1> &vector)
    {
        m_rotation.setColumn1(vector);
    }

    template <typename OtherType> inline void setColumn2 (const Eigen::Matrix<OtherType,3,1> &vector)
    {
        m_rotation.setColumn2(vector);
    }

    template<typename OtherType> inline void setAngleAxis (const double &angle, const Eigen::Matrix<OtherType,3,1> &vector)
    {
        m_rotation = Quaternion<T>::fromAngleAxis(angle, vector);
    }

    template<typename OtherType> inline void setX(const OtherType &x)
    {
        m_translation.x() = x;
    }

    template<typename OtherType> inline void setY(const OtherType &y)
    {
        m_translation.y() = y;
    }

    template<typename OtherType> inline void setZ(const OtherType &z)
    {
        m_translation.z() = z;
    }

    template<typename OtherType> inline static Transform fromAngleAxis (const double &angle, const Eigen::Matrix<OtherType,3,1> &vector)
    {
        return Transform(Quaternion<T>::fromAngleAxis(angle, vector));
    }

    inline static Transform fromDHtable (T theta, T d, T a, T alpha)
    {
        return Transform(cos(theta), -sin(theta)*cos(alpha),  sin(theta)*sin(alpha), cos(theta)*a,
                         sin(theta),  cos(theta)*cos(alpha), -cos(theta)*sin(alpha), sin(theta)*a,
                                  0,             sin(alpha),             cos(alpha),            d);
    }

    template<typename OtherType> inline static Transform fromVector6_RPY (const Eigen::Matrix<OtherType,6,1> &vector)
    {
        return Transform(Rotmat<T>::fromRPY(vector[3], vector[4], vector[5]),
                         Vector3<T>(vector[0], vector[1], vector[2]));
    }

    template<typename OtherType> inline static Transform fromVector6_Euler (const Eigen::Matrix<OtherType,6,1> &vector,
                                                                            const Axis &axis1, const Axis &axis2, const Axis &axis3)
    {
        return Transform(Rotmat<T>::fromEuler(vector[3], vector[4], vector[5], axis1, axis2, axis3),
                         Vector3<T>(vector[0], vector[1], vector[2]));
    }

// Functions =======================================================================================================

    template <typename OtherType> inline Transform<OtherType> cast () const
    {
        Transform<OtherType> Tr;
        Tr.setRotation(Rotmat<OtherType>(m_rotation.template cast<OtherType>()));
        Tr.setTranslation(Vector3<OtherType>(m_translation.template cast<OtherType>()));
        return Tr;
    }

    inline Transform inv () const
    {
        Rotmat<T> Rinv = m_rotation.inv();
        return Transform(Rinv, Vector3<T>(-Rinv*m_translation));
    }

    inline Transform transformTo (const Transform &T1) const
    {
        return T1*inv();
    }

    inline void setIdentity()
    {
        (*this) = Transform::Identity();
    }

    inline bool hasNaN() const
    {
        return (m_rotation.hasNaN() || m_translation.hasNaN());
    }

    inline bool isnormal() const
    {
        return (m_rotation.isnormal() && m_translation.isnormal());
    }

    inline bool isfinite() const
    {
        return (m_rotation.isfinite() && m_translation.isfinite());
    }

    inline static Transform minJerkInterpolation (const Transform &Ti, const Transform &Tf, const double &t)
    {
        Quaternion<T> Qi = Ti.getQuaternion();
        Quaternion<T> Qf = Tf.getQuaternion();
        Vector3<T> Vi = Ti.getTranslation();
        Vector3<T> Vf = Tf.getTranslation();

        return Transform(Quaternion<T>::slerp(Qi, Qf, t), Vector3<T>::minJerkInterpolation(Vi, Vf, t));
    }

    inline static Transform minVelocityInterpolation (const Transform &Ti, const Transform &Tf, const double &t)
    {
        Quaternion<T> Qi = Ti.getQuaternion();
        Quaternion<T> Qf = Tf.getQuaternion();
        Vector3<T> Vi = Ti.getTranslation();
        Vector3<T> Vf = Tf.getTranslation();

        return Transform(Quaternion<T>::slerp(Qi, Qf, t), Vector3<T>::minVelocityInterpolation(Vi, Vf, t));
    }

    inline static Transform minAccelerationInterpolation (const Transform &Ti, const Transform &Tf, const double &t)
    {
        Quaternion<T> Qi = Ti.getQuaternion();
        Quaternion<T> Qf = Tf.getQuaternion();
        Vector3<T> Vi = Ti.getTranslation();
        Vector3<T> Vf = Tf.getTranslation();

        return Transform(Quaternion<T>::slerp(Qi, Qf, t), Vector3<T>::minAccelerationInterpolation(Vi, Vf, t));
    }

    enum
    {
        MinimumJerk,
        MinimumVelocity,
        MinimumAcceleration
    };

    inline static Transform interpolation (const Transform &Ti, const Transform &Tf, const double &t, const int interpolationType)
    {
        if (interpolationType == MinimumJerk)
        {
            return minJerkInterpolation(Ti, Tf, t);
        }
        else if (interpolationType == MinimumAcceleration)
        {
            return minAccelerationInterpolation(Ti, Tf, t);
        }
        else if (interpolationType == MinimumVelocity)
        {
            return minVelocityInterpolation(Ti, Tf, t);
        }
        ERL_ASSERT(false && "invalid interpolation type");
        return Tf;
    }

    inline static Transform Identity ()
    {
        return Transform(Rotmat<T, ERL_MAJOR_TYPE_VALUE>::Identity(), Vector3<T>::Zero());
    }

    inline static Transform NaN ()
    {
        return Transform(Rotmat<T, ERL_MAJOR_TYPE_VALUE>::NaN(), Vector3<T>::NaN());
    }

    inline static Transform Zero ()
    {
        return Transform(Rotmat<T, ERL_MAJOR_TYPE_VALUE>::Zero(), Vector3<T>::Zero());
    }

    inline static Transform Random (const double &scaling = 1.0)
    {
        return Transform(Rotmat<T, ERL_MAJOR_TYPE_VALUE>::Random(), Vector3<T>::Random(scaling));
    }

    inline static bool verifyOrthogonal(const Transform& _M, const T& _thres)
    {
        return Rotmat<T, ERL_MAJOR_TYPE_VALUE>::verifyOrthogonal(_M.getRotation(),_thres);
    }
    inline static Transform nearestOrthogonal(const Transform & _M)
    {
           return Transform(Rotmat<T, ERL_MAJOR_TYPE_VALUE>::nearestOrthogonal(_M.getRotation() ), _M.getTranslation());
    }
};

}

#undef ERL_TEMPLATE_ARGS
#undef ERL_MAJOR_TYPE_VALUE
#undef ERL_CLASS_ARGS
#undef ERL_MAJOR_TYPE
