/*
 * Copyright (c) 2013-2017 Gauthier Gras <gauthier.gras@gmail.com>
 * Copyright (c) 2013-2017 Konrad Leibrandt <konrad.lei@gmx.de>
 * Licensed under the MIT license. See the license file LICENSE.
*/

#ifndef ERL_CIRCULARBUFFER_H
#define ERL_CIRCULARBUFFER_H

#include <vector>
#include <chrono>

namespace Erl
{

template <typename T>
class CircularBuffer
{

public:

    CircularBuffer (const unsigned& size, const T& defaultValue = T())
        : m_data(size, defaultValue)
        , m_index(0)
    {}

    inline virtual void set(const T& value)
    {
        m_data[m_index] = value;
        m_index++;
        if (m_index == m_data.size())
            m_index = 0;
    }

    inline unsigned size() const { return m_data.size(); }

    inline T operator [] (unsigned i) const
    {
        unsigned readIndex = m_index + i;
        if (readIndex >= m_data.size())
            readIndex -= m_data.size();
        return m_data[readIndex];
    }

    inline T first () const
    {
        return m_data[m_index];
    }

    inline T last () const
    {
        if (m_index == 0)
            return m_data[m_data.size() - 1];
        else
            return m_data[m_index - 1];
    }

protected:

    std::vector<T> m_data;
    unsigned m_index;

};

template <typename T>
class TimedCircularBuffer : public CircularBuffer<T>
{

public:

    inline TimedCircularBuffer (const unsigned& size, const T& defaultValue = T())
        : CircularBuffer<T>(size, defaultValue)
        , m_timestamps(size, -1)
        , m_start(std::chrono::high_resolution_clock::now())
    {}

    inline virtual void set(const T& value)
    {
        this->m_data[this->m_index] = value;
        m_t = std::chrono::high_resolution_clock::now() - m_start;
        m_timestamps[this->m_index] = m_t.count();

        this->m_index++;
        if (this->m_index == this->m_data.size())
            this->m_index = 0;
    }

    inline std::vector<T> getPrevious_ns(const int64_t& ns) const
    {
        m_t = std::chrono::high_resolution_clock::now() - m_start;
        int64_t t_curr = m_t.count();

        unsigned startIndex;
        if (this->m_index == 0)
            startIndex = this->m_data.size() - 1;
        else
            startIndex = this->m_index - 1;
        unsigned index = startIndex;
        for (unsigned i = 0; i < this->m_data.size() - 1; i++)
        {
            if ( t_curr - m_timestamps[index] > ns )
                break;
            if (index == 0)
                index = this->m_data.size() - 1;
            else
                index--;
        }


        if (index == startIndex)
        {
            return std::vector<T>();
        }
        else
        {
            unsigned beginIndex = index + 1;
            if (beginIndex == this->m_data.size())
                beginIndex = 0;

            if (beginIndex <= startIndex)
            {
                std::vector<T> ret(startIndex - beginIndex + 1);
                for (unsigned i = beginIndex; i <= startIndex; i++)
                    ret[i - beginIndex] = this->m_data[i];
                return ret;
            }
            else
            {
                std::vector<T> ret( (startIndex + 1) + (this->m_data.size() - beginIndex) );
                unsigned counter = 0;
                for (unsigned i = beginIndex; i < this->m_data.size(); i++)
                {
                    ret[counter] = this->m_data[i];
                    counter++;
                }
                for (unsigned i = 0; i <= startIndex; i++)
                {
                    ret[counter] = this->m_data[i];
                    counter++;
                }
                return ret;
            }
        }

    }

    inline T getNearestTo_ns(const int64_t& ns) const
    {
        m_t = std::chrono::high_resolution_clock::now() - m_start;
        int64_t t_curr = m_t.count();

        unsigned startIndex;
        if (this->m_index == 0)
            startIndex = this->m_data.size() - 1;
        else
            startIndex = this->m_index - 1;
        unsigned index = startIndex;
        for (unsigned i = 0; i < this->m_data.size() - 1; i++)
        {
            if ( t_curr - m_timestamps[index] > ns )
                break;
            if (index == 0)
                index = this->m_data.size() - 1;
            else
                index--;
        }

        if (index == startIndex)
            return this->m_data[startIndex];
        index++;
        if (index == this->m_data.size())
            return this->m_data[0];
        else
            return this->m_data[index];
    }

    inline T getLatest() const
    {
        return this->last();
    }

    inline T getOldest() const
    {
        return this->first();
    }

    inline std::vector<T> getPrevious_us(const int64_t& us) const
    {
        return getPrevious_ns(us * (int64_t)1000);
    }

    inline std::vector<T> getPrevious_ms(const int64_t& ms) const
    {
        return getPrevious_ns(ms * (int64_t)1000000);
    }

    inline T getNearestTo_us(const int64_t& us) const
    {
        return getNearestTo_ns(us * (int64_t)1000);
    }

    inline T getNearestTo_ms(const int64_t& ms) const
    {
        return getNearestTo_ns(ms * (int64_t)1000000);
    }

protected:

    std::vector<int64_t> m_timestamps;
    std::chrono::time_point<std::chrono::high_resolution_clock, std::chrono::nanoseconds> m_start;
    mutable std::chrono::nanoseconds m_t;

};

}

#endif
