/*
 * Copyright (c) 2013-2017 Gauthier Gras <gauthier.gras@gmail.com>
 * Copyright (c) 2013-2017 Konrad Leibrandt <konrad.lei@gmx.de>
 * Licensed under the MIT license. See the license file LICENSE.
*/

#ifndef ERL_CALLBACKMANAGER
#define ERL_CALLBACKMANAGER

#include <list>
#include <mutex>

namespace Erl
{

namespace details
{

template <typename... Types>
class CallbackContainerBase
{

public:

    inline virtual ~CallbackContainerBase() {}
    virtual void handleCallback(Types&&... args) = 0;
};

template<class F, typename... Types>
class CallbackContainer : public CallbackContainerBase<Types...>
{

public:

    inline virtual ~CallbackContainer() {}

    inline void createCallback(F function)
    {
        m_func = function;
    }

    inline void handleCallback(Types&&... args)
    {
        *m_func(std::forward<Types>(args)...);
    }

private:

    F m_func;
};

template<class F, class Class, typename... Types>
class CallbackClassContainer : public CallbackContainerBase<Types...>
{

public:

    inline virtual ~CallbackClassContainer() {}

    inline void createCallback(F function, Class *classInstance)
    {
        m_func = function;
        m_instance = classInstance;
    }

    inline void handleCallback(Types&&... args)
    {
        (m_instance->*m_func)(std::forward<Types>(args)...);
    }

private:

    F m_func;
    Class *m_instance;
};

}

template <typename lockable, typename... Types>
class CallbackManager_t
{

public:

    inline CallbackManager_t()
        : m_callbacks(new std::list<details::CallbackContainerBase<Types...>*>()) {}

    inline ~CallbackManager_t()
    {
        typename std::list<details::CallbackContainerBase<Types...>*>::iterator it;
        for (it = m_callbacks->begin(); it != m_callbacks->end(); ++it)
            delete (*it);
        delete m_callbacks;
    }

    template<class F> inline int createCallback(F function)
    {
        details::CallbackContainer<F, Types...> *container = new details::CallbackContainer<F, Types...>;
        container->createCallback(function);

        m_handleLock.lock();
        m_callbacks->push_back(container);
        int ret = m_callbacks->size() - 1;
        m_handleLock.unlock();
        return ret;
    }

    template<class F, class Class> inline int createCallback(F function, Class *classInstance)
    {
        details::CallbackClassContainer<F, Class, Types...> *container = new details::CallbackClassContainer<F, Class, Types...>;
        container->createCallback(function, classInstance);

        m_handleLock.lock();
        m_callbacks->push_back(container);
        int ret = m_callbacks->size() - 1;
        m_handleLock.unlock();
        return ret;
    }

    inline void handleCallbacks(Types&&... args)
    {
        m_handleLock.lock();

        typename std::list<details::CallbackContainerBase<Types...>*>::iterator it;
        for (it = m_callbacks->begin(); it != m_callbacks->end(); ++it)
        {
            (*it)->handleCallback(std::forward<Types>(args)...);
        }

        m_handleLock.unlock();
    }

    inline int getNumberOfCallbacks() const
    {
        m_handleLock.lock();
        int ret = m_callbacks->size();
        m_handleLock.unlock();
        return ret;
    }

    inline void deleteCallback(const unsigned &index)
    {
        m_handleLock.lock();
        int toClear = std::min(index, (unsigned)m_callbacks->size() - 1);
        typename std::list<details::CallbackContainerBase<Types...>*>::iterator it = m_callbacks->begin();
        std::advance(it, toClear);
        delete (*it);
        m_callbacks->erase(it);
        m_handleLock.unlock();
    }

    inline void deleteAllCallbacks()
    {
        m_handleLock.lock();
        typename std::list<details::CallbackContainerBase<Types...>*>::iterator it;
        for (it = m_callbacks->begin(); it != m_callbacks->end(); ++it)
        {
            delete (*it);
        }
        m_callbacks->clear();
        m_handleLock.unlock();
    }

    std::list<details::CallbackContainerBase<Types...>*> *m_callbacks;
    mutable lockable m_handleLock;
};

template<typename... Types> using CallbackManager = CallbackManager_t<std::mutex, Types...>;

}

#endif

